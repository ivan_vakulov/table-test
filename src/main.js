import Vue from 'vue'
import App from './App.vue'

import router from './router'
import store from './storage'
import '../node_modules/vue-material/dist/vue-material.css'

new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app')

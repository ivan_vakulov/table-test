import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import persons from './modules/persons/persons'

const state = {

};

const getters = {

};

const mutations = {

};

const actions = {

};

const modules = {
    persons
};

export default new Vuex.Store({
    strict: true,
    state,
    mutations,
    getters,
    actions,
    modules,
});

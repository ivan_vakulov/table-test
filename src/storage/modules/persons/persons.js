import Vue from 'vue';
import { SET_DATA, UPDATE_PERSON, DELETE_PERSON } from './persons-mutation-types';
import data from '../../../assets/initialData.json';

const state = {
    persons: [],
    titles: {},
};

const getters = {
    getPersons: state => state.persons,
    getPerson: state => id => state.persons.find(person => person.id === id),
    getTitles: state => state.titles,
};

const mutations = {
    [SET_DATA](state, data) {
        state.persons = data.items.map((e, index) => {
            e.id = index;
            return e;
        });
        state.titles = data.titles;
    },
    [UPDATE_PERSON](state, updatedPerson) {
        let index = state.persons.findIndex(person => person.id === updatedPerson.id);
        Vue.set(state.persons, index, updatedPerson);
        localStorage.setItem('data', JSON.stringify({
            items: state.persons,
            titles: state.titles,
            time: Date.now(),
        }));
    },
    [DELETE_PERSON](state, personToDelete) {
        let index = state.persons.findIndex(person => person.id === personToDelete.id);
        state.persons.splice(index, 1)
    },
};

const actions = {
    setData({commit}) {
        try {
            const storageData = JSON.parse(localStorage.getItem('data'));
            if (Date.now() - storageData.time > 3 * 3600 * 1000) {
                commit(SET_DATA, data);
                data.time = Date.now();
                localStorage.setItem('data', JSON.stringify(data));
            } else {
                commit(SET_DATA, storageData);
            }
        } catch (e) {
            commit(SET_DATA, data);
            data.time = Date.now();
            localStorage.setItem('data', JSON.stringify(data));
        }
    },
    updatePerson({commit}, person) {
        commit(UPDATE_PERSON, person);
    },
    deletePerson({commit}, person) {
        commit(DELETE_PERSON, person);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

import App from '../App';
import Persons from '../components/Persons';
import Person from '../components/Person';

export default new Router({
    routes: [
        {
            path: '/persons',
            component: App,
            children: [
                { path: '', name: 'persons-table', component: Persons },
                { path: ':id', name: 'person', component: Person },
            ],
        },

        { path: '*', redirect: '/persons' },
    ],
});

